@echo off

set "MinGW_w64_DIR=%SystemDrive%\Tools\x64\MSYS2\mingw64"
set "MinGW_w64_BINARY_DIR=%MinGW_w64_DIR%\bin"

set "PKG_CONFIG_PATH=%MinGW_w64_DIR%\lib\pkgconfig"

set "INCLUDE=%MinGW_w64_DIR%\include"
set     "LIB=%MinGW_w64_DIR%\lib"

set "PATH=%MinGW_w64_BINARY_DIR%;%PATH%"
