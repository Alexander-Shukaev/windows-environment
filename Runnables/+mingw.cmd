@echo off

if not defined MinGW_BINARY_DIR (
  set "MinGW_BINARY_DIR=%MinGW_DIR%\bin"
)

if not defined MinGW_INCLUDE_DIR (
  set "MinGW_INCLUDE_DIR=%MinGW_DIR%\include"
)

if not defined MinGW_LIBRARY_DIR (
  set "MinGW_LIBRARY_DIR=%MinGW_DIR%\lib"
)

if exist "%MinGW_BINARY_DIR%\" (
  set "PATH=%MinGW_BINARY_DIR%;%PATH%"
)

if exist "%MinGW_INCLUDE_DIR%\" (
  set "INCLUDE=%MinGW_INCLUDE_DIR%;%INCLUDE%"
)

if exist "%MinGW_LIBRARY_DIR%\" (
  set "LIB=%MinGW_LIBRARY_DIR%;%LIB%"
)

if exist "%MinGW_LIBRARY_DIR%\pkgconfig\" (
  set "PKG_CONFIG_PATH=%MinGW_LIBRARY_DIR%\pkgconfig;%PKG_CONFIG_PATH%"
)
