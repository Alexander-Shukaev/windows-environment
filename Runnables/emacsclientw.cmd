@echo off

setlocal

pushd "%MYFILES%"

call "+msys2.cmd"
call "+mingw.cmd"
call "+home.cmd"

popd

if not defined Emacs_BINARY_DIR (
  set "Emacs_BINARY_DIR=%Emacs_DIR%\bin"
)

"%Emacs_BINARY_DIR%\emacsclientw.exe" %*

endlocal
