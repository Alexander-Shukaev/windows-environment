@echo off

if not defined MSYS2_BINARY_DIR (
  set "MSYS2_BINARY_DIR=%MSYS2_DIR%\usr\bin"
)

if exist "%MSYS2_BINARY_DIR%\" (
  set "PATH=%MSYS2_BINARY_DIR%;%PATH%"
)

if exist "%MSYS2_BINARY_DIR%\core_perl\" (
  set "PATH=%MSYS2_BINARY_DIR%\core_perl;%PATH%"
)

if exist "%MSYS2_BINARY_DIR%\vendor_perl\" (
  set "PATH=%MSYS2_BINARY_DIR%\vendor_perl;%PATH%"
)

if exist "%MSYS2_BINARY_DIR%\site_perl\" (
  set "PATH=%MSYS2_BINARY_DIR%\site_perl;%PATH%"
)
