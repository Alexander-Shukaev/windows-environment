@echo off

set "Perl_DIR=%SystemDrive%\Toolchains\%ARCH%\Perl\5.18.2"
set "Perl_BINARY_DIR=%Perl_DIR%\bin"

set "PATH=%Perl_BINARY_DIR%;%PATH%"
