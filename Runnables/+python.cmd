@echo off

set "Python27_DIR=%SystemDrive%\Toolchains\%ARCH%\Python\2.7.6"
set "Python27_SCRIPTS_DIR=%Python27_DIR%\Scripts"

set "Python34_DIR=%SystemDrive%\Toolchains\%ARCH%\Python\3.4.0"
set "Python34_SCRIPTS_DIR=%Python34_DIR%\Scripts"

set "Python2_DIR=%Python27_DIR%"
set "Python2_SCRIPTS_DIR=%Python27_SCRIPTS_DIR%"

set "Python3_DIR=%Python34_DIR%"
set "Python3_SCRIPTS_DIR=%Python34_SCRIPTS_DIR%"

set "Python_DIR=%Python2_DIR%"
set "Python_SCRIPTS_DIR=%Python2_SCRIPTS_DIR%"

set "PY_PYTHON2=2.7"
set "PY_PYTHON3=3.4"
set "PY_PYTHON=2.7"

set "PATH=%Python_DIR%;%Python_SCRIPTS_DIR%;%PATH%"
