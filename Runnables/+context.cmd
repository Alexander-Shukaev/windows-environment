@echo off

set "ConTeXt_DIR=%SystemDrive%\Tools\x64\ConTeXt\tex\texmf-win64"
set "ConTeXt_BINARY_DIR=%ConTeXt_DIR%\bin"

set "PATH=%ConTeXt_BINARY_DIR%;%PATH%"
