@echo off

set "Lua_DIR=%SystemDrive%\Toolchains\%ARCH%\Lua\5.2.3"
set "Lua_BINARY_DIR=%Lua_DIR%\bin"

set "PATH=%Lua_BINARY_DIR%;%PATH%"
