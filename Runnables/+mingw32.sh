#!/bin/sh

export MinGW_w64_DIR="/${SYSTEMDRIVE:0:1}/Tools/x64/MSYS2/mingw32"
export MinGW_w64_BINARY_DIR="${MinGW_w64_DIR}/bin"

export PKG_CONFIG_PATH="${MinGW_w64_DIR}/lib/pkgconfig"

export INCLUDE="${MinGW_w64_DIR}/include"
export     LIB="${MinGW_w64_DIR}/lib"

export PATH="${MinGW_w64_BINARY_DIR}:${PATH}"
