@echo off

setlocal

call "+msys2.cmd"
call "+llvm.cmd"
call "+python.cmd"
call "+ruby.cmd"
call "+lua.cmd"
call "+perl.cmd"
call "+uncrustify.cmd"

"%SystemDrive%\Applications\%ARCH%\Vim\vim.exe" %*

endlocal
