@echo off

set "LLVM_DIR=%SystemDrive%\Toolchains\%ARCH%\LLVM\3.4"
set "LLVM_BINARY_DIR=%LLVM_DIR%\bin"

set "PATH=%LLVM_BINARY_DIR%;%PATH%"
